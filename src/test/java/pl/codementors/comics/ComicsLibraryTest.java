package pl.codementors.comics;


import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
public class ComicsLibraryTest {

    private ComicsLibrary library;
    private Comic comic1, comic2, comic3, comic4, comic5, comic6, comic7, comic8, comic9;

    @Before
    public void init() {
        library = new ComicsLibrary();
        comic1 = new Comic("Batman 1", "Bob Kane","Batman", Comic.Cover.SOFT, 1939, 1);
        comic2 = new Comic("Batman 2", "Bob Kane","Batman", Comic.Cover.SOFT, 1939, 2);
        comic3 = new Comic("Batman 3", "Bill Finger","Batman", Comic.Cover.SOFT, 1939, 3);
        comic4 = new Comic("Superman 1", "Jerry Siegel","Superman", Comic.Cover.SOFT, 1938, 1);
        comic5 = new Comic("Superman 13", "Jerry Siegel","Superman", Comic.Cover.SOFT, 1939, 1);
        comic6 = new Comic("Tytul", "Autor", "Seria", Comic.Cover.SOFT, 1939, 5);
        comic7 = new Comic("Tytul2", "Autor2", "Seria2;", Comic.Cover.SOFT, 2000, 9);
        comic8 = new Comic("Tytul3", "Autor3", "Seria3", Comic.Cover.SOFT, 1950, 1);
        comic9 = new Comic("Tytul4", "Autor4", "Seria4", Comic.Cover.SOFT, 1950, 1);

    }

    @Test
    public void ComicLibrary_creation_createsInstance() {
        assertNotNull(library);
    }

    @Test
    public void ComicLibrary_creation_createsEmptyCollection() {
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void add_nullParam_doesNothing() {
        library.add(null);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void add_notNullParam_addsComic() {
        library.add(comic1);
        assertThat(library.getComics().isEmpty(), is(false));
        assertThat(library.getComics().size(), is(1));
        assertThat(library.getComics(), hasItem(comic1));
    }

    @Test
    public void remove_nullParam_doesNothing() {
        library.remove(comic1);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void remove_notAddedComic_doesNothing() {
        library.remove(comic1);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void remove_addedComic_removesCommic() {
        library.add(comic1);
        library.remove(comic1);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void changeCovers_softCoversPresentHardCoverProvided_changesAllToHardCover() {
        library.add(comic1);
        library.add(comic2);
        library.changeCovers(Comic.Cover.HARD);
        assertEquals(comic1.getCover(), Comic.Cover.HARD);
        assertEquals(comic2.getCover(), Comic.Cover.HARD);
    }

    @Test
    public void changeCovers_hardCoversPresentSoftCoverProvided_changesAllToHardCover() {
        comic1.setCover(Comic.Cover.HARD);
        comic2.setCover(Comic.Cover.HARD);
        library.add(comic1);
        library.add(comic2);
        library.changeCovers(Comic.Cover.SOFT);
        assertEquals(comic1.getCover(), Comic.Cover.SOFT);
        assertEquals(comic2.getCover(), Comic.Cover.SOFT);
    }

    @Test
    public void getAuthors_noComicsAdded_emptyCollection() {
        assertThat(library.getAuthors().isEmpty(), is(true));
    }

    @Test
    public void getAuthors_oneComicAdded_oneAuthor() {
        library.add(comic1);
        assertThat(library.getAuthors().size(), is(1));
        assertThat(library.getAuthors(), hasItem(comic1.getAuthor()));
    }

    @Test
    public void getAuthors_twoComicsWithDifferentAuthorsAdded_twoAuthors() {
        library.add(comic1);
        library.add(comic3);
        assertThat(library.getAuthors().size(), is(2));
        assertThat(library.getAuthors(), hasItem(comic1.getAuthor()));
        assertThat(library.getAuthors(), hasItem(comic3.getAuthor()));
    }

    @Test
    public void getAuthors_threeComicsWIthTwoDifferentAuthorsAdded_twoAuthors() {
        library.add(comic1);
        library.add(comic2);
        library.add(comic3);
        assertThat(library.getAuthors().size(), is(2));
        assertThat(library.getAuthors(), hasItem(comic1.getAuthor()));
        assertThat(library.getAuthors(), hasItem(comic3.getAuthor()));
    }

    @Test
    public void getSeries_noComicsAdded_emptyCollection() {
        assertThat(library.getSeries().isEmpty(), is(true));
    }

    @Test
    public void getSeries_oneComicAdded_oneSeries() {
        library.add(comic1);
        assertThat(library.getSeries().size(), is(1));
        assertThat(library.getSeries(), hasItem(comic1.getSeries()));
    }

    @Test
    public void getSeries_twoComicsAddedWithDifferentSeries_twoSeries() {
        library.add(comic1);
        library.add(comic4);
        assertThat(library.getSeries().size(), is(2));
        assertThat(library.getSeries(), hasItem(comic1.getSeries()));
        assertThat(library.getSeries(), hasItem(comic4.getSeries()));
    }

    @Test
    public void getSeries_threeComicsWithTwoDifferentSeries_twoSeries() {
        library.add(comic1);
        library.add(comic2);
        library.add(comic4);
        assertThat(library.getSeries().size(), is(2));
        assertThat(library.getSeries(), hasItem(comic1.getSeries()));
        assertThat(library.getSeries(), hasItem(comic4.getSeries()));
    }

    @Test
    public void load_fileDoesNotExit_doNothing() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(false);
        library.load(file);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void load_fileIsDirectory_doNothing() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(true);
        when(file.isFile()).thenReturn(false);
        library.load(file);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void load_fileCannotBeRead_doNothing() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(false);
        when(file.isFile()).thenReturn(true);
        when(file.canRead()).thenReturn(false);
        library.load(file);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    @PrepareForTest(ComicsLibrary.class)
    public void load_fileIsEmpty_doNothing() throws Exception {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(false);
        when(file.isFile()).thenReturn(true);
        when(file.canRead()).thenReturn(true);
        FileReader fr = mock(FileReader.class);
        Scanner scanner = mock(Scanner.class);
        whenNew(FileReader.class).withArguments(file).thenReturn(fr);
        whenNew(Scanner.class).withArguments(fr).thenReturn(scanner);
        when(scanner.nextInt()).thenThrow(NoSuchElementException.class);
        when(scanner.hasNextInt()).thenReturn(false);
        library.load(file);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    @PrepareForTest(ComicsLibrary.class)
    public void load_fileHasOneComic_addOneComic() throws Exception {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(false);
        when(file.isFile()).thenReturn(true);
        when(file.canRead()).thenReturn(true);
        FileReader fr = mock(FileReader.class);
        Scanner scanner = mock(Scanner.class);
        whenNew(FileReader.class).withArguments(file).thenReturn(fr);
        whenNew(Scanner.class).withArguments(fr).thenReturn(scanner);
        when(scanner.hasNextInt()).thenReturn(true).thenReturn(false);
        when(scanner.nextInt()).thenReturn(1).thenReturn(1).thenReturn(1939);
        when(scanner.nextLine()).thenReturn("Batman 1").thenReturn("Bob Kane").thenReturn("Batman");
        when(scanner.next()).thenReturn("SOFT");
        library.load(file);
        assertThat(library.getComics().size(), is(1));
        assertThat(library.getComics(), hasItem(comic1));
    }

    @Test
    @PrepareForTest(ComicsLibrary.class)
    public void load_fileHasTwoComics_addTwoComics() throws Exception {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(false);
        when(file.isFile()).thenReturn(true);
        when(file.canRead()).thenReturn(true);
        FileReader fr = mock(FileReader.class);
        Scanner scanner = mock(Scanner.class);
        whenNew(FileReader.class).withArguments(file).thenReturn(fr);
        whenNew(Scanner.class).withArguments(fr).thenReturn(scanner);
        when(scanner.hasNextInt()).thenReturn(true).thenReturn(false);
        when(scanner.nextInt()).thenReturn(2).thenReturn(1).thenReturn(1939).thenReturn(1).thenReturn(1938);
        when(scanner.nextLine()).thenReturn("Batman 1").thenReturn("Bob Kane").thenReturn("Batman")
                .thenReturn("Superman 1").thenReturn("Jerry Siegel").thenReturn("Superman");
        when(scanner.next()).thenReturn("SOFT").thenReturn("SOFT");
        library.load(file);
        assertThat(library.getComics().size(), is(2));
        assertThat(library.getComics(), hasItem(comic1));
        assertThat(library.getComics(), hasItem(comic4));
    }

    @Test
    public void countBySeries_noComics_return0() {
        assertEquals(library.countBySeries("Batman"), 0);
    }

    @Test
    public void countBySeries_threeComicsTwoFromOneSeries_returnNumberOfComicsInSeries() {
        library.add(comic1);
        library.add(comic2);
        library.add(comic4);
        assertEquals(library.countBySeries("Batman"), 2);
        assertEquals(library.countBySeries("Superman"), 1);
    }

    @Test
    public void countByAuthor_noComics_return0() {
        assertEquals(library.countBySeries(comic1.getAuthor()), 0);
    }

    @Test
    public void countByAuthor_threeComicsTwoFromOneAuthor_returnNumberOfComicsWitAuthor() {
        library.add(comic1);
        library.add(comic2);
        library.add(comic4);
        assertEquals(library.countByAuthor(comic1.getAuthor()), 2);
        assertEquals(library.countByAuthor(comic4.getAuthor()), 1);
    }

    @Test
    public void countByYear_noComics_return0() {
        assertEquals(library.countByYear(comic1.getPublishYear()), 0);
    }

    @Test
    public void countByYear_threeComicsTwoFromOneYear_returnNumberOfComicsFromYear() {
        library.add(comic1);
        library.add(comic2);
        library.add(comic4);
        assertEquals(library.countByYear(comic1.getPublishYear()), 2);
        assertEquals(library.countByYear(comic4.getPublishYear()), 1);
    }

    @Test
    public void countByYearAndMonth_noComics_return0() {
        assertEquals(library.countByYearAndMonth(comic1.getPublishYear(), comic1.getPublishMonth()), 0);
    }

    @Test
    public void countByYearAndMonth_threeComicsTwoFromOneYearAndMonth_returnNumberOfComicsFromYearAndMonth() {
        library.add(comic1);
        library.add(comic5);
        library.add(comic4);
        assertEquals(library.countByYearAndMonth(comic1.getPublishYear(), comic1.getPublishMonth()), 2);
        assertEquals(library.countByYearAndMonth(comic4.getPublishYear(), comic4.getPublishMonth()), 1);
    }

    @Test
    public void removeOlderThan_noOlderComics_doNothing() {
        library.add(comic1);
        library.add(comic4);
        library.removeAllOlderThan(1938);
        assertThat(library.getComics().size(), is(2));
    }

    @Test
    public void removeOlderThan_onlyOlderComics_removeAll() {
        library.add(comic1);
        library.add(comic4);
        library.removeAllOlderThan(1940);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void removeOlderThan_twoComicsOneOlder_removeOne() {
        library.add(comic1);
        library.add(comic4);
        library.removeAllOlderThan(1939);
        assertThat(library.getComics().size(), is(1));
        assertThat(library.getComics(), hasItem(comic1));
    }

    @Test
    public void removeNewerThan_onlyNewerComics_removeAll(){
        library.add(comic1);
        library.add(comic4);
        library.removeAllNewerThan(1937);
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void removeNewerThan_twoComicsOneNewer_removeOne(){
        library.add(comic1);
        library.add(comic4);
        library.removeAllNewerThan(1938);
        assertThat(library.getComics().size(), is(1));
        assertThat(library.getComics(), hasItem(comic4));
    }

    @Test
    public void removeNewerThan_noNewerComics_doNothing(){
        library.add(comic1);
        library.add(comic4);
        library.removeAllNewerThan(1939);
        assertThat(library.getComics().size(), is(2));
    }

    @Test
    public void removeAllFromYearAndMouth_noComicFromYearAndMouth_removeAll(){
    library.add(comic1);
    library.add(comic5);
    library.removeAllFromYearAndMouth(1939, 1);
    assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void removeAllFromYearAndMouth_noComicsFromYearAndMouth_doNothing() {
        library.add(comic1);
        library.add(comic6);
        library.removeAllFromYearAndMouth(comic7.getPublishYear(), comic7.getPublishMonth());
        assertThat(library.getComics().size(), is(2));
        assertThat(library.getComics(), hasItem(comic1));
        assertThat(library.getComics(), hasItem(comic6));
    }

    @Test
    public void removeAllFromYearAndMouth_twoComic_removeOne(){
        library.add(comic1);
        library.add(comic7);
        library.removeAllFromYearAndMouth(1939, 1);
        assertThat(library.getComics().size(), is(1));
        assertThat(library.getComics(), hasItem(comic7));
    }

    public void removeAllFromYearAndMouth_DiffYearOneMouth_doNothing(){
        library.add(comic1);
        library.add(comic4);
        library.add(comic8);
        library.removeAllFromYearAndMouth(1945, 1);
        assertThat(library.getComics(), hasItem(comic1));
        assertThat(library.getComics(), hasItem(comic4));
        assertThat(library.getComics(), hasItem(comic8));
    }

    public void removeAllFromYearAndMouth_DiffMouthOneYear_doNothing(){
        library.add(comic1);
        library.add(comic2);
        library.add(comic3);
        library.removeAllFromYearAndMouth(1939, 4);
        assertThat(library.getComics(), hasItem(comic1));
        assertThat(library.getComics(), hasItem(comic2));
        assertThat(library.getComics(), hasItem(comic3));
    }

    @Test
    public void removeAllFromAuthor_noComicsFromAuthor_doNothing() {
        library.add(comic1);
        library.add(comic4);
        library.removeAllFromAuthor(comic3.getAuthor());
        assertThat(library.getComics().size(), is(2));
        assertThat(library.getComics(), hasItem(comic1));
        assertThat(library.getComics(), hasItem(comic4));
    }

    @Test
    public void removeAllFromAuthor_allComicsFromAuthor_removeAll() {
        library.add(comic1);
        library.add(comic2);
        library.removeAllFromAuthor(comic1.getAuthor());
        assertThat(library.getComics().isEmpty(), is(true));
    }

    @Test
    public void removeAllFromAuthor_twoComicsOneFromAuthor_removeOne() {
        library.add(comic1);
        library.add(comic4);
        library.removeAllFromAuthor(comic1.getAuthor());
        assertThat(library.getComics().size(), is(1));
        assertThat(library.getComics(), hasItem(comic4));
    }

    @Test
    public void getAuthorsComics_noComics_emptyMap() {
        assertThat(library.getAuthorsComics().isEmpty(), is(true));
    }


    @Test
    public void getAuthorsComics_fiveComicsTwoFromOneAuthorTwoFromSecondOneFromThird_mapWithProperValues() {
        library.add(comic1);
        library.add(comic2);
        library.add(comic3);
        library.add(comic4);
        library.add(comic5);
        Map<String, Collection<Comic>> authorsComics = library.getAuthorsComics();
        assertThat(authorsComics.size(), is(3));
        assertThat(authorsComics.keySet(), hasItems(comic1.getAuthor(), comic3.getAuthor(), comic4.getAuthor()));
        assertThat(authorsComics.get(comic1.getAuthor()).size(), is(2));
        assertThat(authorsComics.get(comic3.getAuthor()).size(), is(1));
        assertThat(authorsComics.get(comic4.getAuthor()).size(), is(2));
        assertThat(authorsComics.get(comic1.getAuthor()), hasItems(comic1, comic2));
        assertThat(authorsComics.get(comic3.getAuthor()), hasItems(comic3));
        assertThat(authorsComics.get(comic4.getAuthor()), hasItems(comic4, comic5));
    }

    @Test
    public void getYearsComics_fiveComicsFourFromOneYearOneFromSecond_mapWithProperValues() {
        library.add(comic1);
        library.add(comic2);
        library.add(comic3);
        library.add(comic4);
        library.add(comic5);
        Map<Integer, Collection<Comic>> yearsComics = library.getYearsComics();
        assertThat(yearsComics.size(), is(2));
        assertThat(yearsComics.keySet(), hasItems(comic1.getPublishYear(), comic4.getPublishYear()));
        assertThat(yearsComics.get(comic1.getPublishYear()).size(), is(4));
        assertThat(yearsComics.get(comic4.getPublishYear()).size(), is(1));
        assertThat(yearsComics.get(comic1.getPublishYear()), hasItems(comic1, comic2, comic3, comic5));
        assertThat(yearsComics.get(comic4.getPublishYear()), hasItems(comic4));
    }


    @Test
    public void getYearsMouthComics_twoComicsSameYearDiffMouths_mapWithProperValues(){
        library.add(comic1);
        library.add(comic2);
        Pair firstPair = new ImmutablePair(comic1.getPublishYear(), comic1.getPublishMonth());
        Pair secondPair = new ImmutablePair(comic2.getPublishYear(), comic2.getPublishMonth());
        Map<Pair<Integer, Integer>, Collection<Comic>> pairComic = library.getYearsMouthComics();
        assertThat(pairComic.size(), is(2));
        assertThat(pairComic.keySet(), hasItems(firstPair,secondPair));
        assertThat(pairComic.get(firstPair), hasItems(comic1));
        assertThat(pairComic.get(secondPair), hasItems(comic2));
    }

    @Test
    public void getYearsMouthComics_twoComicsDiffYearsSameMouth_mapWithProperValues(){
        library.add(comic1);
        library.add(comic4);
        Pair firstPair = new ImmutablePair(comic1.getPublishYear(), comic1.getPublishMonth());
        Pair secondPair = new ImmutablePair(comic4.getPublishYear(), comic4.getPublishMonth());
        Map<Pair<Integer, Integer>, Collection<Comic>> pairComic = library.getYearsMouthComics();
        assertThat(pairComic.size(), is(2));
        assertThat(pairComic.keySet(), hasItems(firstPair,secondPair));
        assertThat(pairComic.get(firstPair), hasItems(comic1));
        assertThat(pairComic.get(secondPair), hasItems(comic4));
    }

    @Test
    public void getYearsMouthComics_twoComicsDiffYearsDiffMouths_mapWithProperValues(){
        library.add(comic1);
        library.add(comic7);
        Pair firstPair = new ImmutablePair(comic1.getPublishYear(), comic1.getPublishMonth());
        Pair secondPair = new ImmutablePair(comic7.getPublishYear(), comic7.getPublishMonth());
        Map<Pair<Integer, Integer>, Collection<Comic>> pairComic = library.getYearsMouthComics();
        assertThat(pairComic.size(), is(2));
        assertThat(pairComic.keySet(), hasItems(firstPair,secondPair));
        assertThat(pairComic.get(firstPair), hasItems(comic1));
        assertThat(pairComic.get(secondPair), hasItems(comic7));
    }

    @Test
    public void getYearsMouthComics_twoComicsSameYearSameMouth_mapWithProperValues(){
        library.add(comic8);
        library.add(comic9);
        Pair firstPair = new ImmutablePair(comic8.getPublishYear(), comic8.getPublishMonth());
        Map<Pair<Integer, Integer>, Collection<Comic>> pairComic = library.getYearsMouthComics();
        assertThat(pairComic.size(), is(1));
        assertThat(pairComic.keySet(), hasItems(firstPair));
        assertThat(pairComic.get(firstPair), hasItems(comic8, comic9));
    }

    @Test
    public void getYearsMouthComics_manyComicsDiffYearsDiffMouths_mapWithProperValues(){
        library.add(comic8);
        library.add(comic9);
        library.add(comic7);
        library.add(comic6);
        library.add(comic5);
        library.add(comic4);
        library.add(comic3);
        library.add(comic2);
        library.add(comic1);
        Pair firstPair = new ImmutablePair(comic8.getPublishYear(), comic8.getPublishMonth());
        Pair secondPair = new ImmutablePair(comic1.getPublishYear(), comic1.getPublishMonth());
        Pair thirdPair = new ImmutablePair(comic2.getPublishYear(), comic2.getPublishMonth());
        Pair fourthPair = new ImmutablePair(comic3.getPublishYear(), comic3.getPublishMonth());
        Pair fifthPair = new ImmutablePair(comic4.getPublishYear(), comic4.getPublishMonth());
        Pair sixthPair = new ImmutablePair(comic6.getPublishYear(), comic6.getPublishMonth());
        Pair seventhPair = new ImmutablePair(comic7.getPublishYear(), comic7.getPublishMonth());
        Map<Pair<Integer, Integer>, Collection<Comic>> pairComic = library.getYearsMouthComics();
        assertThat(pairComic.size(), is(7));
        assertThat(pairComic.keySet(), hasItems(firstPair, secondPair, thirdPair, fourthPair, fifthPair, sixthPair, seventhPair));
        assertThat(pairComic.get(firstPair), hasItems(comic8, comic9));
        assertThat(pairComic.get(secondPair), hasItems(comic1, comic5));
        assertThat(pairComic.get(thirdPair), hasItems(comic2));
        assertThat(pairComic.get(fourthPair), hasItems(comic3));
        assertThat(pairComic.get(fifthPair), hasItems(comic4));
        assertThat(pairComic.get(sixthPair), hasItems(comic6));
        assertThat(pairComic.get(seventhPair), hasItems(comic7));

    }
}
