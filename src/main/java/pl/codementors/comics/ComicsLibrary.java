package pl.codementors.comics;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     *
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {
        return comics;
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {

        if (comic != null) {
            comics.add(comic);
        }
    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
       comics.remove(comic);
    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {
        for (Comic comic : comics){
            comic.setCover(cover);
        }
    }

    /**
     *
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {
        Set<String> result = new TreeSet<>();
        for (Comic comic : comics) {
            if(comic.getAuthor() != null){
                result.add(comic.getAuthor());
            }
        }
        return result;
    }

    /**
     *
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {
        Set<String> result = new TreeSet<>();
        for (Comic comic : comics) {
            if(comic.getSeries() != null){
                result.add(comic.getSeries());
            }
        }
        return result;
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     *
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     *
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     *
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {

        try (FileReader fileReader = new FileReader(file);
             Scanner scanner = new Scanner(fileReader);){

            int numerOfComics = scanner.nextInt();
            scanner.skip("\n");

            for (int i = 0; i < numerOfComics; i++){

                Comic comic = new Comic();
                String title = scanner.nextLine();
                String author = scanner.nextLine();
                String series = scanner.nextLine();
                String cover = scanner.next();
                int publishMouth = scanner.nextInt();
                int publishYear = scanner.nextInt();
                scanner.skip("\n");

                comic.setTitle(title);
                comic.setAuthor(author);
                comic.setSeries(series);
                comic.setCover(Comic.Cover.valueOf(cover));
                comic.setPublishMonth(publishMouth);
                comic.setPublishYear(publishYear);

                comics.add(comic);

            }

        }catch(RuntimeException|IOException e){
            System.out.println(":)");
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {

        int count = 0;

        for(Comic comic :comics){
            if(series != null && series.equals(comic.getSeries())){
                count++;
            }
        }
        return count;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {

        int count = 0;

        for(Comic comic :comics){
            if(author != null && author.equals(comic.getAuthor())){
                count++;
            }
        }
        return count;
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        int count = 0;

        for(Comic comic :comics){
            if(year == comic.getPublishYear()){
                count++;
            }
        }
        return count;
    }

    /**
     * Counts all comics with the same provided publish year and month.
     *
     * @param year Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {

        int count = 0;

        for(Comic comic :comics){
            if(year == comic.getPublishYear() && month == comic.getPublishMonth()){
                count++;
            }
        }
        return count;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year) {

        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            if(comic.getPublishYear() < year ){
                iterator.remove();
            }
        }
    }

    public void removeAllNewerThan(int year){

        Iterator<Comic> iterator = comics.iterator();
        while(iterator.hasNext()){
            Comic comic = iterator.next();
            if(comic.getPublishYear() > year){
                iterator.remove();
            }
        }
    }

    public void removeAllFromYearAndMouth(int year, int mouth){
        Iterator<Comic> iterator = comics.iterator();
        while(iterator.hasNext()){
            Comic comic = iterator.next();
            if((comic.getPublishYear() == year) && (comic.getPublishMonth() == mouth)){
                iterator.remove();
            }
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {
        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            if(comic.getAuthor() != null && comic.getAuthor().equals(author)){
                iterator.remove();
            }
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {

        Map<String, Collection<Comic>> map = new TreeMap<>();
        for (Comic comic : comics){
            if (!map.containsKey(comic.getAuthor())){
                ArrayList list = new ArrayList();
                list.add(comic);
                map.put(comic.getAuthor(), list);
            } else {
                Collection<Comic> list = map.get(comic.getAuthor());
                list.add(comic);
                map.put(comic.getAuthor(), list);
            }
        }
        return map;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {

        Map<Integer, Collection<Comic>> map = new TreeMap<>();
        for(Comic comic :comics){
            if(!map.containsKey(comic.getPublishYear())){
                ArrayList list = new ArrayList();
                list.add(comic);
                map.put(comic.getPublishYear(), list);
            }else{
                Collection<Comic> list = map.get(comic.getPublishYear());
                list.add(comic);
                map.put(comic.getPublishYear(), list);
            }
        }
        return map;
    }

    public Map<Pair<Integer , Integer>, Collection<Comic>> getYearsMouthComics() {

        Map<Pair<Integer, Integer>, Collection<Comic>> map = new TreeMap<>();

        for (Comic comic : comics) {
            Pair pair = new ImmutablePair(comic.getPublishYear(), comic.getPublishMonth());

            if (!map.containsKey(pair)) {
                ArrayList list = new ArrayList();
                list.add(comic);
                map.put(pair, list);
            }else{
                Collection<Comic> list = map.get(pair);
                list.add(comic);
                map.put(pair, list);
            }
        }
        return map;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComicsLibrary that = (ComicsLibrary) o;

        return getComics() != null ? getComics().equals(that.getComics()) : that.getComics() == null;
    }

    @Override
    public int hashCode() {
        return getComics() != null ? getComics().hashCode() : 0;
    }
}







