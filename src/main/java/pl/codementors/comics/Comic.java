package pl.codementors.comics;

import java.io.Serializable;

/**
 * Representation of single comics in library. Describes comics title, author, cover type, publish year and month.
 *
 * @author psysiu
 */
public class Comic implements Serializable {

    /**
     * Describes comics cover type.
     */
    public enum Cover {
        HARD,
        SOFT;
    }

    /**
     * Comic title.
     */
    private String title;

    /**
     * Comic author.
     */
    private String author;

    /**
     * Comics series.
     */
    private String series;

    /**
     * Publish year. Must be positive integer from 1867 (date of first serialized comics for a mass audience)
     * - 2017 range.
     */
    private int publishYear;

    /**
     * Publish month. Must be positive integer from 1 - 12 range.
     */
    private int publishMonth;

    /**
     * Comic cover.
     */
    private Cover cover;

    /**
     * Default constructor. Sets title and author to empty strings, publish year to 1867,
     * publish month to 1 and cover to soft.
     */
    public Comic() {
        title = "";
        author = "";
        publishYear = 1867;
        publishMonth = 1;
        cover = Cover.SOFT;
       //throw new UnsupportedOperationException();
    }

    /**
     * Creates new comics. If publish year is not in range 1867 - 2017, the 1867 value is used. If publish month
     * is not in range 1 - 12, the 1 value is used.
     *
     * @param title Comic title.
     * @param author Comic author.
     * @param cover Comic cover.
     * @param series Comics series.
     * @param publishYear Comic publish year (1867 - 2017).
     * @param publishMonth Comic publish month (1 - 12).
     */
    public Comic(String title, String author, String series, Cover cover, int publishYear, int publishMonth) {

        this.title = title;
        this.author = author;
        this.series = series;
        this.cover = cover;

        if((publishYear >= 1867) && (publishYear <= 2017)){
            this.publishYear = publishYear;
        } else {
            this.publishYear = 1867;
        }

        if((publishMonth >= 1) && (publishMonth <= 12)){
            this.publishMonth = publishMonth;
        } else {
            this.publishMonth = 1;
        }

      //  throw new UnsupportedOperationException();
    }

    /**
     * @return Comic title.
     */
    public String getTitle() {
        return title;

       // throw new UnsupportedOperationException();
    }

    /**
     * @param title New value for comics title.
     */
    public void setTitle(String title) {
       this.title = title;

        // / throw new UnsupportedOperationException();
    }

    /**
     * @return Comic publish year.
     */
    public int getPublishYear() {
        return publishYear;

        //throw new UnsupportedOperationException();
    }

    /**
     * Sets new value for publish year. If new value is not in range 1867 - 2017 it does nothing.
     *
     * @param publishYear New value for comics publish year.
     */
    public void setPublishYear(int publishYear) {

       if ((publishYear >= 1867) && (publishYear <= 2017)){
           this.publishYear = publishYear;
       }

        //throw new UnsupportedOperationException();
    }

    /**
     *
     * @return Comic publish month.
     */
    public int getPublishMonth() {
        return publishMonth;
        //throw new UnsupportedOperationException();
    }

    /**
     * Sets new value for publish month. If new value is not in range 1 - 12 it does nothing.
     * @param publishMonth New value for comics publish month.
     */
    public void setPublishMonth(int publishMonth) {

        if((publishMonth >= 1) && (publishMonth <=12)){
            this.publishMonth = publishMonth;
        }

        // /throw new UnsupportedOperationException();
    }

    /**
     *
     * @return Comic author.
     */
    public String getAuthor() {
        return author;

        //throw new UnsupportedOperationException();
    }

    /**
     *
     * @param author New value for comics author.
     */
    public void setAuthor(String author) {
        this.author = author;

        //throw new UnsupportedOperationException();
    }

    /**
     *
     * @return Comic cover.
     */
    public Cover getCover() {
        return cover;

        //throw new UnsupportedOperationException();
    }

    /**
     *
     * @param cover New value for comics.
     */
    public void setCover(Cover cover) {
        this.cover = cover;

       // throw new UnsupportedOperationException();
    }

    /**
     *
     * @return Comic series.
     */
    public String getSeries() {
        return series;
        //throw new UnsupportedOperationException();
    }

    /**
     *
     * @param series New value for comic series.
     */
    public void setSeries(String series) {
        this.series = series;

        //throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comic comic = (Comic) o;

        if (getPublishYear() != comic.getPublishYear()) return false;
        if (getPublishMonth() != comic.getPublishMonth()) return false;
        if (getTitle() != null ? !getTitle().equals(comic.getTitle()) : comic.getTitle() != null) return false;
        if (getAuthor() != null ? !getAuthor().equals(comic.getAuthor()) : comic.getAuthor() != null) return false;
        if (getSeries() != null ? !getSeries().equals(comic.getSeries()) : comic.getSeries() != null) return false;
        return getCover() == comic.getCover();
    }

    @Override
    public int hashCode() {
        int result = getTitle() != null ? getTitle().hashCode() : 0;
        result = 31 * result + (getAuthor() != null ? getAuthor().hashCode() : 0);
        result = 31 * result + (getSeries() != null ? getSeries().hashCode() : 0);
        result = 31 * result + getPublishYear();
        result = 31 * result + getPublishMonth();
        result = 31 * result + (getCover() != null ? getCover().hashCode() : 0);
        return result;
    }
}
